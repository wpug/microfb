# Projekt indywidualny – `microFB`

Z założenia `microFB` ma mieć charakter „prywatny” – dostęp do treści mają jedynie użytkownicy oraz (będący użytkownikami) ich znajomi.

## Elementy do zaimplementowania

### „Ściana”:
    * możliwość zamieszczania wpisów u siebie, jak i u znajomych
    * możliwość „dzielenia się” wpisami znajomych na swojej „ścianie” (zarówno pojedynczymi, jak i „śledzenie wpisów” wybranych znajomych)
    * możliwość „polubienia” wpisów znajomych

Do „ściany” użytkownika dostęp ma jedynie on sam oraz jego znajomi. Widok użytkownika udostępnia dodatkowe informacje – np. stan zalogowania znajomych, powiadomienia o ich nowych wpisach, wiadomościach z komunikatora itp.

### Obsługa „kręgu znajomych”:
    * możliwość wyszukiwania wśród użytkowników `microFB` (dostęp mamy jedynie do podstawowych danych na temat użytkownika)
    * możliwość zapraszania do kręgu znajomych (i potwierdzania zaproszeń)
### Komunikator `microFB`:
    * możliwość rozmów 1-1 ze znajomymi
    * możliwość rozmów grupowych ze znajomymi

## Technologie,które należy wykorzystać

  - [Node.js](https://nodejs.org/en/)
  - [Express](https://expressjs.com/)
  - [Less](http://lesscss.org/)
  - [Socket.io](https://socket.io/)
  - [Passport.js](http://passportjs.org/)
  - [MongoDB](https://www.mongodb.com/)

## Wymagania techniczne

  - responsywny interfejs użytkownika
